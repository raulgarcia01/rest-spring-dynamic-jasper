package mx.com.etl.dynamic.report.ws;

import ar.com.fdvs.dj.core.DynamicJasperHelper;
import ar.com.fdvs.dj.core.layout.ClassicLayoutManager;
import ar.com.fdvs.dj.domain.DynamicReport;
import ar.com.fdvs.dj.domain.builders.ColumnBuilderException;
import ar.com.fdvs.dj.domain.builders.FastReportBuilder;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import mx.com.etl.dynamic.report.model.Alumnos;
import mx.com.etl.dynamic.report.repository.AlumnosRepository;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Admin
 */
@RestController
@CrossOrigin(origins = "*")
public class AlumnosController {

    //-- Injecta el repositorio que lista informacion de alumnos
    @Autowired
    AlumnosRepository alumnosRepository;

    @RequestMapping(value = "/alumnos", method = RequestMethod.GET, produces = "application/pdf")
    public ResponseEntity<InputStreamResource> alumnosPDFReport(@RequestParam(value = "nombre", defaultValue = "", required = false) String nombre)
            throws IOException, JRException, ColumnBuilderException, ClassNotFoundException {

        //-- Insercion de alumnos
        this.alumnosRepository.deleteAll();
        this.alumnosRepository.save(new Alumnos("Juan", "Perez", "5", "10", "8", "10"));
        this.alumnosRepository.save(new Alumnos("Maria", "Mendoza", "6", "8", "8", "9"));
        this.alumnosRepository.save(new Alumnos("Jorge", "Funes", "5", "8", "4", "5"));
        this.alumnosRepository.save(new Alumnos("Hector", "Hernandez", "5", "7", "6", "7"));
        this.alumnosRepository.save(new Alumnos("Geovanni", "Galeas", "5", "9", "9", "10"));

        FastReportBuilder drb = new FastReportBuilder();
        InputStream pdfFile = null;
        if (nombre != null && !nombre.equals("")) {
            DynamicReport dr = drb.addColumn("Nombre", "nombre", String.class.getName(), 50)
                    .addColumn("Apellido", "apellido", String.class.getName(), 50)
                    .addColumn("Edad", "edad", String.class.getName(), 30)
                    .addColumn("Nota 1", "nota1", String.class.getName(), 30)
                    .addColumn("Nota 2", "nota2", String.class.getName(), 30)
                    .addColumn("Nota 3", "nota3", String.class.getName(), 30)
                    .setTitle("Listado de Notas de Alumnos")
                    .setSubtitle("Reporte generado" + new Date())
                    .setPrintBackgroundOnOddRows(true)
                    .setUseFullPageWidth(true)
                    .build();
            JRDataSource ds = new JRBeanCollectionDataSource(this.alumnosRepository.findAll());
            JasperPrint jp = DynamicJasperHelper.generateJasperPrint(dr, new ClassicLayoutManager(), ds);
            pdfFile = new ByteArrayInputStream(JasperExportManager.exportReportToPdf(jp));
        } else {
            DynamicReport dr = drb.addColumn("Apellido", "apellido", String.class.getName(), 50)
                    .addColumn("Edad", "edad", String.class.getName(), 30)
                    .addColumn("Nota 1", "nota1", String.class.getName(), 30)
                    .addColumn("Nota 2", "nota2", String.class.getName(), 30)
                    .addColumn("Nota 3", "nota3", String.class.getName(), 30)
                    .setTitle("Listado de Notas de Alumnos")
                    .setSubtitle("Reporte generado" + new Date())
                    .setPrintBackgroundOnOddRows(true)
                    .setUseFullPageWidth(true)
                    .build();
            JRDataSource ds = new JRBeanCollectionDataSource(this.alumnosRepository.findAll());
            JasperPrint jp = DynamicJasperHelper.generateJasperPrint(dr, new ClassicLayoutManager(), ds);
            pdfFile = new ByteArrayInputStream(JasperExportManager.exportReportToPdf(jp));
        }

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "inline; filename=" + "test.pdf");
        headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
        headers.add("Pragma", "no-cache");
        headers.add("Expires", "0");

        return ResponseEntity
                .ok()
                .headers(headers)
                .contentLength(pdfFile.available())
                .contentType(MediaType.parseMediaType("application/pdf"))
                .body(new InputStreamResource(pdfFile));
       
    }

}
