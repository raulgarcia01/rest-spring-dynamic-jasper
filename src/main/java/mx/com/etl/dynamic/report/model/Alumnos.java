package mx.com.etl.dynamic.report.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

/**
 *
 * @author rgarcia
 */
@Entity
@Data
@Table(name = "alumnos_table")
public class Alumnos implements Cloneable, Serializable {

    private static final long serialVersionUID = 5887252366986466801L;

    public Alumnos(String nombre, String apellido, String edad, String nota1, String nota2, String nota3) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.edad = edad;
        this.nota1 = nota1;
        this.nota2 = nota2;
        this.nota3 = nota3;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "nombre")
    private String nombre;

    @Column(name = "apellido")
    private String apellido;

    @Column(name = "edad")
    private String edad;

    @Column(name = "nota1")
    private String nota1;

    @Column(name = "nota2")
    private String nota2;

    @Column(name = "nota3")
    private String nota3;

}
