package mx.com.etl.dynamic.report.repository;

import mx.com.etl.dynamic.report.model.Alumnos;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Admin
 */
@Repository
public interface AlumnosRepository extends JpaRepository<Alumnos, Integer> {
    
}
