package mx.com.etl.dynamic.report.main;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan("mx.com.etl.dynamic.report.ws")
@EntityScan("mx.com.etl.dynamic.report.model")
@EnableJpaRepositories("mx.com.etl.dynamic.report.repository")
public class WsApplication {

    private static final Logger log = LoggerFactory.getLogger(WsApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(WsApplication.class, args);
    }
}
